using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGiveScore
{
   void AddScore(int _score);
}
