using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    [SerializeField] GameObject Host;
    [SerializeField] private int damage = 1;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject == Host) return;


        if (collider.gameObject.TryGetComponent<IDamageable>(out var _damageable))
        {
            _damageable.ApplyDamage(Host, damage);
        }
        
    }

}
