using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Player : MonoBehaviour, IDamageable
{
    public UnityEvent<int> onHpChanged;

    private const int MaxHp = 5;
    [SerializeField] PlayerController playerCtrl;

    [Header("Health")] 
    [SerializeField] private int currentHp = MaxHp;
    
    
    [SerializeField] private Transform spawnPoint;

    


    private void Start()
    {
        onHpChanged?.Invoke(currentHp);
    }
    
    #region HP

    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }
    


    private void Death()
    {
        
    }

    private void Respawn()
    {
        playerCtrl.rb.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }

    public void ApplyDamage(GameObject _source, int _damage)
    {
        currentHp -= _damage;
        if (currentHp <= 0)
        {
            Death(_source);
        }
        onHpChanged?.Invoke(currentHp);
    }

    protected virtual void Death(GameObject _source)
    {
        currentHp = MaxHp;
        Respawn();
    }
    #endregion


}
