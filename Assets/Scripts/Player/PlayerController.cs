using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] AttackController attackCtrl;

    [Header("Movement")]
    [SerializeField] private Rigidbody2D _rb;
    [SerializeField] private Transform body;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float jumpForce = 7f;
    [SerializeField] private LayerMask groundLayer;

    public Rigidbody2D rb 
    {
        get { return _rb; }
        set { _rb = value; }
    }

    private Vector2 moveInput;
    private bool isGrounded;


    public void OnMove(InputAction.CallbackContext _context)
    {
        moveInput = _context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext _context)
    {
        if (!isGrounded) return;

        _rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        isGrounded = false;
    }

    private readonly float checkGroundRayLenght = 0.6f;

    private void FixedUpdate()
    {
        //UpdateMovement
        _rb.velocity = new Vector2(moveInput.x * moveSpeed, _rb.velocity.y);

        // Flip the player sprite when changing direction
        if (moveInput.x != 0)
        {
            body.localScale = new Vector3(Mathf.Sign(moveInput.x), 1f, 1f);
            float _rotate = Mathf.Sign(moveInput.x) > 0 ? 0 : 180f;
            attackCtrl.firePoint.rotation = Quaternion.Euler(0, 0, _rotate);
        }

        //CheckGround
        RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, checkGroundRayLenght, groundLayer);

        isGrounded = _hit.collider != null;
    }

    private void UpdateMovement()
    {

    }

    private void CheckGround()
    {

    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, Vector3.down * checkGroundRayLenght, Color.green);
    }
}
