using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StatUIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI hpText;
    [SerializeField] private TextMeshProUGUI weaponText;
    [SerializeField] private TextMeshProUGUI scoreText;

    [SerializeField] private AttackController player;

    private void Start()
    {
        
    }

    private void OnEnable()
    {
        ScoreManager.Instance.OnScoreChanged += UpdateScore;
        AttackController.OnChangedWeapon += UpdateWeapon;
    }

    private void OnDestroy()
    {
        if (ScoreManager.Instance)
        {
            ScoreManager.Instance.OnScoreChanged -= UpdateScore;
        }
        AttackController.OnChangedWeapon -= UpdateWeapon;
    }

    public void UpdateHp(int _hp)
    {
        hpText.SetText($"HP: {_hp}");
    }

    private void UpdateWeapon(WeaponData _weapon)
    {
        weaponText.SetText($"Weapon: {_weapon.Name}");
    }
    
    private void UpdateScore(int _score)
    {
        scoreText.SetText($"Score: {_score}");
    }
}
